defmodule DiscussWeb.TopicController do
  use DiscussWeb, :controller

  alias Discuss.Topics.Topic
  alias Discuss.Repo

  def index(conn, _params) do
    topics_list = Repo.all(Topic)

    render(conn, "index.html", topics: topics_list)
  end

  def new(conn, _params) do
    changeset = Topic.changeset(%Topic{}, %{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"topic" => topic_string}) do
    changeset = Topic.changeset(%Topic{}, topic_string)

    case Repo.insert(changeset) do
      {:ok, _topic_string} ->
        conn
        |> put_flash(:info, "Topic Created")
        |> redirect(to: Routes.topic_path(conn, :index))

      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => topic_id}) do
    edited_topic = Repo.get(Topic, topic_id)
    changeset = Topic.changeset(edited_topic)

    render(conn, "edit.html", changeset: changeset, topic: edited_topic)
  end

  def update(conn, %{"id" => topic_id, "topic" => topic_string}) do
    old_topic = Repo.get(Topic, topic_id)
    changeset = Topic.changeset(old_topic, topic_string)

    case Repo.update(changeset) do
      {:ok, _topic} ->
        conn
        |> put_flash(:info, "Topic title updated")
        |> redirect(to: Routes.topic_path(conn, :index))

      {:error, changeset} ->
        render(conn, "edit.html", changeset: changeset, topic: old_topic)
    end
  end

  def delete(conn, %{"id" => topic_id}) do
    Repo.get!(Topic, topic_id) |> Repo.delete!()

    conn
    |> put_flash(:info, "Topic removed")
    |> redirect(to: Routes.topic_path(conn, :index))
  end
end
