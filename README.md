# Discuss

A discussion generation program which uses Model-View-Controller paradigm. Able to interact with Postgresql for CRUD operations.

## How to run

Make sure your have installed:

- [Elixir](https://elixir-lang.org/)
- [Phoenix](https://www.phoenixframework.org/)
- [Postgresql](https://www.postgresql.org/)

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Additional features for future consideration

- Authentication with OAuth
- [Plugs](https://hexdocs.pm/phoenix/plug.html)
- Websockets